﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatrónSingleton
{
    class Program
    {
        static void Main(string[] args)
        {
            var b1 = EquilibradorDeCarga.GetEquilibradorDeCarga();
            var b2 = EquilibradorDeCarga.GetEquilibradorDeCarga();
            var b3 = EquilibradorDeCarga.GetEquilibradorDeCarga();
            var b4 = EquilibradorDeCarga.GetEquilibradorDeCarga();

            // Se confirma que se trata de la misma instancia

            if (b1 == b2 && b2 == b3 && b3 == b4)
            {
                Console.WriteLine("La misma instancia\n");
            }

            // A continuación, se equilibra la carga de 15 peticiones para un servidor

            var equilibrador = EquilibradorDeCarga.GetEquilibradorDeCarga();
            for (int i = 0; i < 15; i++)
            {
                string NombreDelServidor = equilibrador.SiguienteServidor.Nombre;
                Console.WriteLine("Envía la solicitud a: " + NombreDelServidor);
            }

            // Esperar al usuario
            Console.ReadKey();
        }

        private static void ReadKey()
        {
            throw new NotImplementedException();
        }
    }


    public class EquilibradorDeCarga
    {
        // Los segmentos estáticos se inicializan, es decir, inmediatamente cuando la clase se carga por primera vez, la red garantiza
        // la seguridad de los hilos para la inicialización estática.

        private static readonly EquilibradorDeCarga instancia = new EquilibradorDeCarga();
        private readonly List<Servidor> servidores;
        private readonly Random random = new Random();

        // El constructor viene hacer privado.

        private EquilibradorDeCarga()
        {
            // Se carga la lista de servidores disponibles

            servidores = new List<Servidor>
                {
                  new Servidor{ Nombre = "Servidor1", IP = "120.11.240.18" },
                  new Servidor{ Nombre = "Servidor2", IP = "120.11.240.19" },
                  new Servidor{ Nombre = "Servidor3", IP = "120.11.240.20" },
                  new Servidor{ Nombre = "Servidor4", IP = "120.11.240.21" },
                  new Servidor{ Nombre = "Servidor5", IP = "120.11.240.22" },
                };
        }
        public static EquilibradorDeCarga GetEquilibradorDeCarga()
        {
            return instancia;
        }

        // El equilibrador viene siendo de carga simple

        public Servidor SiguienteServidor
        {
            get
            {
                int r = random.Next(servidores.Count);
                return servidores[r];
            }
        }
    }


    public class Servidor
    {
        public string Nombre { get; set; }
        public string IP { get; set; }
    }
}